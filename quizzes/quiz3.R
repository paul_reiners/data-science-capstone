# Quiz 3: Natural language processing II
source("./src/functions.R")

fraction <- "032"
fourGramModel <- loadFourGramModel(fraction, "all")

# Question 1
phrase <- "When you breathe, I want to be the air for you. I'll be there for you, I'd live and I'd"
choices <- c("eat", "sleep", "die", "give")
predict(fraction, phrase, choices, fourGramModel)

# Question 2
phrase <- "Guy at my table's wife got up to go to the bathroom and I asked about dessert and he started telling me about his"
choices <- c("horticultural", "financial", "marital", "spiritual")
predict(fraction, phrase, choices, fourGramModel)

# Question 3
phrase <- "I'd give anything to see arctic monkeys this"
choices <- c("morning", "month", "decade", "weekend")
predict(fraction, phrase, choices, fourGramModel)

# Question 4
phrase <- "Talking to your mom has the same effect as a hug and helps reduce your"
choices <- c("sleepiness", "happiness", "hunger", "stress")
predict(fraction, phrase, choices, fourGramModel)

# Question 5
phrase <- "When you were in Holland you were like 1 inch away from me but you hadn't time to take a"
choices <- c("look", "minute", "walk", "picture")
predict(fraction, phrase, choices, fourGramModel)

# Question 6
phrase <- "I'd just like all of these questions answered, a presentation of evidence, and a jury to settle the"
choices <- c("account", "matter", "incident", "case")
predict(fraction, phrase, choices, fourGramModel)

# Question 7
phrase <- "I can't deal with unsymetrical things. I can't even hold an uneven number of bags of groceries in each"
choices <- c("hand", "toe", "finger", "arm")
predict(fraction, phrase, choices, fourGramModel)
predict(fraction, phrase, choices, fourGramModel)

# Question 8
phrase <- "Every inch of you is perfect from the bottom to the"
choices <- c("top", "side", "center", "middle")
predict(fraction, phrase, choices, fourGramModel)

# Question 9
phrase <- "I’m thankful my childhood was filled with imagination and bruises from playing"
choices <- c("inside", "daily", "weekly", "outside")
predict(fraction, phrase, choices, fourGramModel)

# Question 10
phrase <- "I like how the same people are in almost all of Adam Sandler's"
choices <- c("pictures", "novels", "stories", "movies")
predict(fraction, phrase, choices, fourGramModel)
