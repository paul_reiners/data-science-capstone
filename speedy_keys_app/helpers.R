library(data.table)
library(R.utils)

ptm <- proc.time()
load("allNGramsDF.rda")
print(proc.time() - ptm)

trim.trailing <- function (x) sub("\\s+$", "", x)

computeNextWord <- function(phrase) {
    words <- strsplit(phrase, " ")
    words <- words[[1]]
    n <- length(words)
    if (n >= 3) {
        nextWord <- compute4Gram(words, n)
    } else if (n >= 2) {
        nextWord <- compute3Gram(words, n)
    } else if (n >= 1) {
        nextWord <- compute2Gram(words, n)
    } else {
        nextWord <- compute1Gram()
    }
    
    trimmedPhrase <- trim.trailing(phrase)
    n <- nchar(trimmedPhrase)
    lastChar <- substr(trimmedPhrase, n, n)
    if (trimmedPhrase == "" || lastChar == "." || lastChar == "?" || lastChar == "!") {
        nextWord <- capitalize(nextWord)
    }
    
    nextWord <- sapply(nextWord, capitalizeI)
    
    return(nextWord)
}

capitalizeI <- function(word) {
    if (word == "i") {
        return("I")
    } else {
        return(word)
    }
}

compute4Gram <- function(words, n) {
    prefix <- tolower(paste(words[n-2], words[n-1], words[n]))
    retVal <- fourGramsDT[prefix,]$word
    if (length(retVal) <= 1 && is.na(retVal)) {
        retVal <- compute3Gram(words, n)
    } else if (length(retVal) == 3) {
        # do nothing
    } else if (length(retVal) == 2 | length(retVal) == 1) {
        augRetVal <- compute3Gram(words, n)
        retVal <- unique(c(retVal, augRetVal))[1:3]
    }  
    
    return(retVal)
}

compute3Gram <- function(words, n) {
    prefix <- tolower(paste(words[n-1], words[n]))
    retVal <- threeGramsDT[prefix,]$word
    if (length(retVal) <= 1 && is.na(retVal)) {
        retVal <- compute2Gram(words, n)
    } else if (length(retVal) == 3) {
        # do nothing
    } else if (length(retVal) == 2 | length(retVal) == 1) {
        augRetVal <- compute2Gram(words, n)
        retVal <- unique(c(retVal, augRetVal))[1:3]
    }  
    
    return(retVal)
}

compute2Gram <- function(words, n) {
    prefix <- tolower(words[n])
    retVal <- twoGramsDT[prefix,]$word
    if (length(retVal) <= 1 && is.na(retVal)) {
        retVal <- compute1Gram()
    } else if (length(retVal) == 3) {
        # do nothing
    } else if (length(retVal) == 2 | length(retVal) == 1) {
        augRetVal <- compute1Gram()
        retVal <- unique(c(retVal, augRetVal))[1:3]
    }  
    
    return(retVal)
}

compute1Gram <- function() {
    return(c("the", "be", "to"))
}
