library(shiny)

shinyUI(fluidPage(theme = "bootstrap.css",
    
    titlePanel("SpeedyKey"),
    
    sidebarLayout(
        sidebarPanel(
            p("SpeedyKey is a clone of SwiftKey written for educational purposes.  It is my capstone project for the ", 
              tags$a(href="https://www.coursera.org/specialization/jhudatascience/1", "Johns Hopkins Data Science Specialization"), "."),
            p("SpeedyKey was designed and coded by Paul Reiners.")
        ),

        mainPanel(
            fluidRow(
                column(4, uiOutput("txtOutput2")),
                column(4, uiOutput("txtOutput1")),
                column(4, uiOutput("txtOutput3"))),
            fluidRow(
                column(
                    12, 
                    tags$textarea(
                        id="txtInput", rows=3, 
                        placeholder="Type your text here or press a button above...")))
        )
    )
))
