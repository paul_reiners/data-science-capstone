---
title: "Task 2 - Exploratory analysis"
author: "Paul Reiners"
date: "July 8, 2015"
output: html_document
---

3. How many unique words do you need in a frequency sorted dictionary to cover 50% of all word instances in the language? 90%? 

```{r echo=FALSE}
library(tm)

source("../../src/functions.R")

sampleFraction <- "064"

dir <- paste('../../data/final/en_US/', sampleFraction, '/', sep = "")
```

```{r}
blogPath <- paste(dir, 'en_US.blogs.txt', sep = "")
blogFreqPath <- paste(dir, 'en_US.blogs.freq.txt', sep = "")
getWordCountForCoverage(blogFreqPath, blogPath, 0.5)
getWordCountForCoverage(blogFreqPath, blogPath, 0.9)

newsPath <- paste(dir, 'en_US.news.txt', sep = "")
newsFreqPath <- paste(dir, 'en_US.news.freq.txt', sep = "")
getWordCountForCoverage(newsFreqPath, newsPath, 0.5)
getWordCountForCoverage(newsFreqPath, newsPath, 0.9)

twitterPath <- paste(dir, 'en_US.twitter.txt', sep = "")
twitterFreqPath <- paste(dir, 'en_US.twitter.freq.txt', sep = "")
getWordCountForCoverage(twitterFreqPath, twitterPath, 0.5)
getWordCountForCoverage(twitterFreqPath, twitterPath, 0.9)
```
