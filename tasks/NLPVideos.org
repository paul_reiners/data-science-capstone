* TODO Coursera course on NLP (not in R)
** DONE Week 1 - Course Introduction
*** DONE Course Introduction
** DONE Week 1 - Basic Text Processing
*** DONE Regular Expressions
*** DONE Regular Expressions in Practical NLP
*** DONE Word Tokenization
*** DONE Word Normalization and Spelling
** DONE Week 1 - Edit Distance
*** DONE Defining Minimum Edit Distance
*** DONE Computing Minimum Edit Distance
*** DONE Backtrace for Computing Alignments
*** DONE Weighted Minimum Edit Distance
*** DONE Minimum Edit Distance in Computational Biology
** DONE Week 2 - Language Modeling
*** DONE Introduction to N-grams
*** DONE Estimating N-gram Probabilities
*** DONE Evaluation and Perplexity
*** DONE Generalization and Zeros
*** DONE Smoothing: Add-One
*** DONE Interpolation
*** DONE Good-Turing Smoothing
*** DONE Kneser-Ney Smoothing
** TODO Week 2 - Spelling Correction
** TODO Week 3 - Text Classification
** TODO Week 3 - Sentiment Analysis
** TODO Week 4 - Discriminative classifiers: Maximum Entropy classifiers
** TODO Week 4 - Named entity recognition and Maximum Entropy Sequence Models
** TODO Week 4 - Relation Extraction
** TODO Week 5 - Advanced Maximum Entropy Models
** TODO Week 5 - POS Tagging
** TODO Week 5 - Parsing Introduction
** TODO Week 5 - Instructor Chat
** TODO Week 6 - Probabilistic Parsing
** TODO Week 6 - Lexicalized Parsing
** TODO Week 7 - Information Retrieval
** TODO Week 7 - Ranked Information Retrieval
** TODO Week 8 - Semantics
** TODO Week 8 - Question Answering
** TODO Week 8 - Summarization
** TODO Week 8 - Instructor Chat II
