import sys
import csv
from os import listdir
from os.path import isfile, join
import pymongo
from pymongo import MongoClient
import io

def mergeNGramFrequencyFiles(files, collection_name):
    client = MongoClient()
    db = client.frequencies_database
    db.drop_collection(collection_name)
    cumFreqs = db[collection_name]
    cumFreqs.create_index([("phrase", pymongo.ASCENDING),
                           ("word", pymongo.ASCENDING)])
    for i in range(len(files)):
        fname = files[i]
        with io.open(fname, encoding="utf8") as f:
            print("Processing", fname)
            j = 0
            for line in f:
                if (j % 100000 == 0):
                    print("     line", j)
                parts = line.split(",")
                phrase = parts[0]
                word = parts[1]
                count = int(parts[2])
                if is_ascii(phrase):
                    record = cumFreqs.find_one({"phrase": phrase, "word": word})
                    if record:
                        currentCount = record["count"]
                        currentCount = currentCount + count
                        record["count"] = currentCount
                        mongo_id = record['_id']
                        cumFreqs.update({'_id': mongo_id}, {"$set": record}, upsert=False)
                    else:
                        new_record = {"phrase": phrase, "word": word, "count": count}
                        cumFreqs.insert_one(new_record)
                j += 1

def merge1GramFrequencyFiles(files, collection_name):
    client = MongoClient()
    db = client.frequencies_database
    db.drop_collection(collection_name)
    cumFreqs = db[collection_name]
    cumFreqs.create_index('word')
    for i in range(len(files)):
        fname = files[i]
        with io.open(fname, encoding="utf8") as f:
            print("Processing", fname)
            j = 0
            for line in f:
                if (j % 10000 == 0):
                    print("     line", j)
                parts = line.split()
                word = parts[0]
                count = int(parts[1])
                if is_ascii(word):
                    record = cumFreqs.find_one({"word": word})
                    if record:
                        currentCount = record["count"]
                        currentCount = currentCount + count
                        record["count"] = currentCount
                        mongo_id = record['_id']
                        cumFreqs.update({'_id': mongo_id}, {"$set": record}, upsert=False)
                    else:
                        new_record = {"word": word, "count": count}
                        cumFreqs.insert_one(new_record)
                j += 1

def create_n_gram_file(collection_name, outputFileName, fraction, n, phrase_name):
    client = MongoClient()
    db = client.frequencies_database
    cumFreqs = db[collection_name]
    cumFreqs.create_index('count')
    total_count = 0
    record_count = 0
    for record in cumFreqs.find():
        if (record_count % 10000 == 0):
            print("     record_count", record_count)
        record_count += 1
        total_count += record["count"]
    with open(outputFileName, 'w') as outFile:
        writer = csv.writer(outFile, lineterminator='\n')
        count_so_far = 0
        desired_count = fraction * total_count
        for record in cumFreqs.find().sort('count', pymongo.DESCENDING):
            if (count_so_far % 10000 == 0):
                print("     count", count_so_far)
                print(count_so_far / desired_count)
            count = record["count"]
            if count == 1:
                break
            phrase = record[phrase_name]
            if n == 1:
                writer.writerow([phrase, count])
            else:
                writer.writerow([phrase, record["word"], count])
            count_so_far += count
            if count_so_far >= desired_count:
                break

def is_ascii(s):
    return all(ord(c) < 128 for c in s)

# Sample call:
# "./data/final/en_US/001/blogs/splits" "./speedy_keys_app/data/blogs/en_US.blog.3.90.freq.csv" "blogs_3_frequencies_collection" "0.90" "3"
def main():
    parent_dir = "./speedy_keys_app/data/"
    blogs_file = "".join([parent_dir, "blogs/en_US.blogs.4.90.freq.csv"])
    news_file = "".join([parent_dir, "news/en_US.news.4.90.freq.csv"])
    twitter_file = "".join([parent_dir, "twitter/en_US.twitter.4.90.freq.csv"])
    files = [blogs_file, news_file, twitter_file]
    collection_name = "all_4_frequencies_collection"
    mergeNGramFrequencyFiles(files, collection_name)
    fraction = 1.0
    n = 4
    outputFileName = "./speedy_keys_app/data/all/en_US.all.4.90.freq.csv"
    create_n_gram_file(collection_name, outputFileName, fraction, n, "phrase")

if __name__ == "__main__":
    main()
